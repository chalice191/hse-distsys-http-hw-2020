import os

from flask import Flask, request, jsonify, abort
from dataclasses import dataclass


app = Flask(__name__)

@dataclass
class User:
	name: str
	age: int

last_id = 0
users = {}

@app.route('/users/', methods=['POST', 'GET'])
def get_users():
	if request.method == 'GET':
		return jsonify({'users': users})

	elif request.method == 'POST':
		global last_id
		new_user = User(request.form['username'], request.form['age'])
		users[last_id] = new_user
		last_id += 1
		return jsonify({'user': new_user})


@app.route('/users/<int:user_id>', methods=['GET', 'PUT', 'PATCH', 'DELETE'])
def edit_user(user_id):
	if user_id not in users:
		abort(404)
	
	if request.method == 'PUT':
		users[user_id].name = request.form['username']
		users[user_id].age = request.form['age']

	elif request.method == 'PATCH':
		if 'username' in request.form:
			users[user_id].name = request.form['username']
		if 'age' in request.form:
			users[user_id].age = request.form['age']
	
	elif request.method == 'DELETE':
		user = users.pop(user_id, {})
		return jsonify({'user': user})

	return jsonify({'user': users[user_id]})


app.run(host='0.0.0.0', port=int(os.environ.get('HSE_HTTP_FLASK_PORT', 80)))
