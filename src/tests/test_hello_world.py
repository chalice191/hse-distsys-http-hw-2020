import os
import requests


SERVER_HOST = os.environ.get('HSE_HTTP_TESTS_SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('HSE_HTTP_FLASK_PORT', 80))
URL = 'http://' + SERVER_HOST
if SERVER_PORT != 80:
    URL += ':{}'.format(SERVER_PORT)


def test_get_users():
	response = requests.get(URL + '/users/')
	assert response.status_code == 200
	assert len(response.json()['users']) == 0

	users = [{'username': 'Momo', 'age': 3},
		{'username': 'Raven', 'age': 300},
		{'username': 'Appa', 'age': 12}]

	for user in users:
		response = requests.post(URL + '/users/', data=user)
		assert response.status_code == 200

	response = requests.get(URL + '/users/')
	assert len(response.json()['users']) == 3

	response = requests.post(URL + '/users/', data={'place': 'UK, London'})
	assert response.status_code != 200

def test_edit_user():
	user_0 = requests.get(URL + '/users/0').json()['user']
	user_1 = requests.put(URL + '/users/0', data={'username': 'Popo', 'age': 5}).json()['user']
	# user_1 = requests.get(URL + '/users/0').json()['user']
	assert user_0['name'] != user_1['name']
	assert user_0['age'] != user_1['age']

	user_2 = requests.patch(URL + '/users/0', data={'age': 13}).json()['user']
	assert user_1['name'] == user_2['name']
	assert user_1['age'] != user_2['age']

def test_delete_user():
	user = requests.get(URL + '/users/2').json()['user']
	deleted = requests.delete(URL + '/users/2').json()['user']
	assert user == deleted

	response = requests.get(URL + '/users/')
	assert len(response.json()['users']) == 2

	response = requests.delete(URL + '/users/2')
	assert response.status_code == 404