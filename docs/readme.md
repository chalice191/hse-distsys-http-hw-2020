### Сервер пользователей

Мой сервер хранит информацию о пользователях - их имена и возраст.

С помощью запроса GET на */users/* можно получить полного списка зарегестрированных пользователей.
POST на тот же адрес добавляет пользователя с указанными данными

GET на */users/id* вернет данные о пользователе с номером id.  
PUT на */users/id* заменит все данные о пользователе с номером id и вернет его новые данные.  
PATCH на */users/id* заменит часть данных о пользователе с номером id и вернет его новые данные.  
DELETE на */users/id* удалит пользователя с номером id и вернет его данные.  